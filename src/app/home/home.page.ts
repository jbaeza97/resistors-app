import { Component } from '@angular/core';

type Entity = number[];

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  
  notInputError = true;
  notMultiplierError = true;
  notMultiplierSet = true;
  ishidden = false;
  isCalculateDisabled = false;
  target = 0; multiplier;
  sets_num = 0;
  error = 'noInput';
  recommendations = [];
  total_resistance = [];
  total_percentage = [];


  resistorValues = [
    1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.7, 3.0, 3.3, 3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1, 10, 11, 12, 13, 15, 16, 18, 20, 22, 24, 27, 30, 33, 36, 39, 43, 47, 51, 56, 62, 68, 75, 82, 91, 1000, 1100, 1200, 1300, 1500, 1600, 1800, 2000, 2200, 2400, 2700, 3000, 3300, 3600, 3900, 4300, 4700, 5100, 5600, 6200, 6800, 7500, 8200, 9100, 10000, 11000, 12000, 13000, 15000, 16000, 18000, 20000, 22000, 24000, 27000, 30000, 33000, 36000, 39000, 43000, 47000, 51000, 56000, 62000, 68000, 75000, 82000, 91000, 100000, 110000, 120000, 130000, 150000, 160000, 180000, 200000, 220000, 240000, 270000, 300000, 330000, 360000, 390000, 430000, 470000, 510000, 560000, 620000, 680000, 750000, 820000, 910000, 1000000, 1100000, 1200000, 1300000, 1500000, 1600000, 1800000, 2000000, 2200000, 2400000, 2700000, 3000000, 3300000, 3600000, 3900000, 4300000, 4700000, 5100000, 5600000, 6200000, 6800000, 7500000, 8200000, 9100000]

  constructor(){}

  calculate(){
    if(this.target == undefined || this.target <= 0){
      this.notInputError = false;
      setTimeout(this.delayNotInputError.bind(this), 1500)
      return
    } else if(this.multiplier == undefined || this.multiplier == 0){
      this.notMultiplierSet = false;
      setTimeout(this.delayNotMultiplierSet.bind(this), 1500)
      return
    } else {}
    this.target = this.target * this.multiplier;
    if (this.target > 9100000){
      this.notMultiplierError = false;
      setTimeout(this.delayNotMultiplierError.bind(this), 1500)
      this.target = 0;
      return
    }
    this.sets_num = 50000 // How many sets to create

    let sets = this.createSets(100)

    let sets_fitness: {
      [fitness: number]: Entity;
    } = { }

    for (let current_set of sets) {
      let fitness = this.fitness(current_set)
      sets_fitness[fitness] = current_set
    }

    // Get all the keys
    let fitness_keys = Object.keys(sets_fitness)

    // Sort the keys
    fitness_keys = fitness_keys.sort()

    // Get the top 3 resistors
    console.log('These are the Best 3 sets found')

    let sets_to_show = 3
    for (let i = 0; i < sets_to_show; ++i) {
      let index_to_show = fitness_keys[i]
      let x = parseFloat(index_to_show)*100
      let n = x.toFixed(3)
      this.total_resistance.push(this.parallelValue(sets_fitness[index_to_show]))
      this.total_percentage.push(String(n)+"%")
      this.recommendations.push(this.convertToResistanceFormat(sets_fitness[index_to_show]))
    }
    this.isCalculateDisabled = !this.isCalculateDisabled
  }

  clear(){
    this.target = 0;
    this.multiplier = 0;
    this.sets_num = 0;
    this.recommendations = [];
    this.total_resistance = [];
    this.total_percentage = [];
    if(this.isCalculateDisabled){
      this.isCalculateDisabled = !this.isCalculateDisabled
    }
  }

  delayNotInputError(){
    this.notInputError = true;
  }
  
  delayNotMultiplierError(){
    this.notMultiplierError = true;
  }

  delayNotMultiplierSet(){
    this.notMultiplierSet = true;
  }

  seed(): Entity {
    let resistor_set = []

    let resist_num = Math.floor(Math.random()*(4)+1)
    for (let i = 0; i < resist_num; ++i) {
      let index = Math.floor(Math.random() * (this.resistorValues.length))
      resistor_set.push(this.resistorValues[index])
    }
    return resistor_set
  }

  createSets(num: Number) : Entity[] {
    let sets = []
    for (let i = 0; i < num; ++i) {
      sets.push(this.seed())
    }

    return sets
  }

  calculateResistance(resistor_set: Entity) {
    let accum = 0
    resistor_set.forEach(function(current_value) {
      accum += 1 / current_value
    })
    return (1 / accum)
  }

  fitness(entity: Entity): number {
    // 0.5% plus for each aditional resistor
    return Math.abs((this.calculateResistance(entity) - this.target) / this.target) + ((entity.length - 1) * 0.005)
  }

  convertToResistanceFormat(arr){
    for(let i = 0; i < arr.length; i++){
      if (arr[i] >= 1000000){
        arr[i] = String(arr[i]/1000000)+"M"
      } else if (arr[i] >= 1000 && arr[i] < 1000000){
        arr[i] = String(arr[i]/1000)+"k"
      } else {
        arr[i] = String(arr[i])
      }
    }
    return arr
  }

  parallelValue(arr){
    let total = 0
    arr.forEach(element => {
      total += (1 / element)
    });
    total = (1/total)
    let temp = total.toFixed(2)
    return temp
  }
}
